#include <iostream>
#include <string>
using namespace std;

int main(){
	string s0("InitialString");
	cout<<s0<<endl;
	
	//default constructor
	string s1;
	
	//copy constructor
	string s2(s0);
	cout<<s2<<endl;
	
	//substring constructor
	string s3(s0,7,6);
	cout<<s3<<endl;
	
	//from c-string
	string s4("ThisIsInitialString");
	cout<<s4<<endl;

	string s5(10,'x');
	cout<<s5<<endl;

	string s6(10,42); // 40 is ascii for *
	cout<<s6<<endl;

	// range constructor
	string s7(s0.begin()+2,s0.end());
	cout<<s7<<endl;
	return 0;
}