#include <iostream>
#include <list>
using namespace std;

class SllNode{
public:
    int data;
    SllNode* next;
};

void Push(SllNode** head,int data){
    SllNode* newNode =  new SllNode;
    newNode->data = data;
    newNode->next = (*head);
    *head = newNode;
}

void printList(SllNode* head){
    SllNode* curr = head;
    while(curr!=NULL){
        cout<<curr->data<<" ";
        curr =  curr->next;
    }
    cout<<endl;
}
SllNode* addition(SllNode* a,SllNode* b){
    SllNode* result = NULL;
    int sum = 0;
    int carry = 0;
    while(a != NULL || b != NULL){
        sum = carry + ((a != NULL)? a->data:0) + ((b != NULL)? b->data:0);
        carry = sum/10;
        sum = sum % 10;
        Push(&result,sum);
        if(a != NULL){
            a = a->next;
        }
        if(b != NULL){
            b = b->next;
        }
    }
    return result;
}
int main(){
    SllNode* a = NULL;
    Push(&a,3);Push(&a,6);Push(&a,5);
    SllNode* b = NULL;
    Push(&b,1);Push(&b,2);Push(&b,4);Push(&b,8);
    SllNode* result = addition(a,b);
    printList(a);
    printList(b);
    printList(result);

}